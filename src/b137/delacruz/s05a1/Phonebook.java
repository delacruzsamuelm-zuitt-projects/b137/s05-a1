package b137.delacruz.s05a1;

/*

In the Phonebook class, create a single instance variable:
- an ArrayList of Contact objects named contacts.
This is to be initialized as an empty ArrayList that can only take in Contact objects.

Define a default constructor and a parameterized constructor for the Phonebook class.
The parameterized constructor will take in a Contact object that will then be added to the contacts ArrayList.

Define getter and setter methods for the Phonebook class.

*/

import java.util.ArrayList;

public class Phonebook {
    // Properties
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    // Empty constructor
    public Phonebook() {
    }

    // Parameterized constructor
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // Getters
    public ArrayList<Contact> getPhonebook() {
        return contacts;
    }

    // Setters
    public void setPhonebook(ArrayList<Contact> phoneBook) {
        this.contacts = phoneBook;
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

    // isEmpty
    public boolean isEmpty() {
        return this.contacts.size() == 0;
    }
}
